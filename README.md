MessengerTestBundle
===================

- Mocks amqp symfony/messenger transport in test environment
- Provides a Behat Context to test for message queues with Ubirak RestApiBehatExtension

Install
-------

    composer require --dev wakeonweb/messenger-test-bundle


Requirements
------------

To use the Behat Context, you must use Ubirak RestApiBehatExtension and the Symfony2BehatExtension, as well as the Symfony Profiler:

    composer require --dev ubirak/rest-api-behat-extension behat/symfony2-extension symfony/profiler-pack

You also need to mock your symfony/messenger amqp queues.
 
Usage
-----

To mock the amqp queues from symfony/messenger, override their configuration in config/packages/test/messenger.yaml:

    framework:
        messenger:
            transports:
                my_queue: 'mock://foo'

Those tests are provided by the MessengerContext:

    Then I should have 1 messages created in my_queue
    Then I should have a message in my_queue with content:
    """
    {
        "foo": "bar"
    }
    """
