<?php

namespace WakeOnWeb\Bundle\MessengerTestBundle\Behat\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Symfony\Component\HttpFoundation\Response;
use Ubirak\RestApiBehatExtension\Rest\RestApiBrowser;

/**
 * Class MessengerContext
 * @package App\Tests\functional\Context
 */
class MessengerContext implements Context
{
    use KernelDictionary;

    /** @var \Ubirak\RestApiBehatExtension\Rest\RestApiBrowser */
    private $restApiBrowser;

    private $profiles = [];

    /**
     * MessengerContext constructor.
     *
     * @param \Ubirak\RestApiBehatExtension\Rest\RestApiBrowser $restApiBrowser
     */
    public function __construct(RestApiBrowser $restApiBrowser)
    {
        $this->restApiBrowser = $restApiBrowser;
    }

    /**
     * @param int    $expectedNbMessages
     * @param string $queueName
     *
     * @Then I should have :nbMessages messages created in :queueName
     * @throws \Exception
     */
    public function iShouldHaveMessagesInQueue(int $expectedNbMessages, string $queueName)
    {
        /** @var \WakeOnWeb\Bundle\MessengerTestBundle\DataCollector\MessengerCollector $messengerCollector */
        $messengerCollector = $this->getProfile()->getCollector('app.messenger');

        if (!isset($messengerCollector->getSentMessages()[$queueName])) {
            throw new \Exception('No queue found with name '.$queueName);
        }

        if ($expectedNbMessages !== ($actualNbMessages = count($messengerCollector->getSentMessages()[$queueName]))) {
            throw new \Exception("Expected $expectedNbMessages on queue $queueName, found $actualNbMessages");
        }
    }

    /**
     * @param string                           $queueName
     * @param \Behat\Gherkin\Node\PyStringNode $content
     *
     * @throws \Exception
     * @Then I should have a message in :queueName with content:
     */
    public function iShouldHaveAMessageInQueueWithContent(string $queueName, PyStringNode $content)
    {
        if (!$this->getContainer()->has('serializer')) {
            throw new \Exception('You must use symfony/serializer to be able to test messages content');
        }

        /** @var \WakeOnWeb\Bundle\MessengerTestBundle\DataCollector\MessengerCollector $messengerCollector */
        $messengerCollector = $this->getProfile()->getCollector('app.messenger');

        if (!isset($messengerCollector->getSentMessages()[$queueName])) {
            throw new \Exception('No queue found with name '.$queueName);
        }

        $expectedContent = json_decode($content->getRaw(), true);
        $errors          = [];

        /** @var \Symfony\Component\Messenger\Envelope $envelope */
        foreach ($messengerCollector->getSentMessages()[$queueName] as $envelope) {
            try {
                $actualContent = $this->getContainer()->get('serializer')->normalize($envelope->getMessage());
                $this->assertArrayMatch($expectedContent, $actualContent);

                return;
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        $message = "No message found with given content.";

        foreach ($errors as $i => $error) {
            $message .= "\nMessage $i:\n".$error;
        }

        throw new \Exception($message);
    }

    /**
     * @param      $expectedArray
     * @param      $actualArray
     * @param null $path
     *
     * @throws \Exception
     */
    private function assertArrayMatch($expectedArray, $actualArray, $path = null)
    {
        foreach ($expectedArray as $key => $value) {
            if (!array_key_exists($key, $actualArray)) {
                $actualKeys = implode(', ', array_keys($actualArray));

                throw new \Exception("Expected key '$key'' was not found in message. Keys present are $actualKeys");
            }

            $actualValue = $actualArray[$key];
            $path        = null === $path ? $path.'['.$key.']' : $key;

            if (is_array($value)) {
                $this->assertArrayMatch($value, $actualValue, $path);
            } elseif ($value !== $actualValue) {
                throw new \Exception("Expected value '$value' for key $path but found '$actualValue''");
            }
        }
    }

    /**
     * @return \Symfony\Component\HttpKernel\Profiler\Profile
     *
     * @throws \Exception
     */
    public function getProfile()
    {
        if (!$this->getContainer()->has('profiler')) {
            throw new \Exception('You must enable symfony/profiler to be able to test messages');
        }

        $guzzleResponse = $this->restApiBrowser->getResponse();

        if (!isset($guzzleResponse->getHeaders()['X-Debug-Token'][0])) {
            throw new \Exception('Profiler header was not found in response. Did you enable the symfony profiler?');
        }

        if (!isset($this->profiles[$token = $guzzleResponse->getHeaders()['X-Debug-Token'][0]])) {
            $symfonyResponse = new Response(
                '',
                $guzzleResponse->getStatusCode(),
                ['X-Debug-Token' => $token,]
            );

            // The profiler looks for a file which can take some time after the response is sent before it's generated
            $iteration = 1;

            do {
                usleep(1000);

                try {
                    $profile = $this->getContainer()->get('profiler')->loadProfileFromResponse($symfonyResponse);
                } catch (\Exception $e) {
                    $profile = null;
                }
            } while (!$profile && $iteration++ < 100);


            $this->profiles[$token] = $profile;
        }

        return $this->profiles[$token];
    }
}
