<?php

namespace WakeOnWeb\Bundle\MessengerTestBundle\DataCollector;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Symfony\Component\Messenger\Transport\TransportInterface;
use WakeOnWeb\Bundle\MessengerTestBundle\Mock\Messenger\Transport;

/**
 * Class MessengerCollector
 * @package App\Tests\DataCollector
 */
class MessengerCollector extends DataCollector
{
    /** @var iterable|\Symfony\Component\Messenger\Transport\TransportInterface[] */
    private $receivers = [];

    /**
     * @param \Symfony\Component\Messenger\Transport\TransportInterface $transport
     * @param                                                           $aliases
     */
    public function addReceiver(TransportInterface $transport, $aliases)
    {
        foreach ($aliases as $alias) {
            $this->receivers[$alias] = $transport;
        }
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request  $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * @param \Exception|null                            $exception
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        foreach ($this->receivers as $name => $transport) {
            if ($transport instanceof Transport) {
                $this->data['receivers'][$name] = $transport->sentMessages;
            }
        }
    }

    /**
     * @return array
     */
    public function getSentMessages()
    {
        return $this->data['receivers'] ?? [];
    }

    public function reset()
    {
        $this->data = [];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app.messenger';
    }
}
