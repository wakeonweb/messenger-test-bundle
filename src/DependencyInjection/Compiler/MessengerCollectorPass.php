<?php

namespace WakeOnWeb\Bundle\MessengerTestBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use WakeOnWeb\Bundle\MessengerTestBundle\DataCollector\MessengerCollector;

/**
 * Class MessengerCollectorPass
 * @package App\Common\DependencyInjection\Compiler
 */
class MessengerCollectorPass implements CompilerPassInterface
{
    /**
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(MessengerCollector::class)) {
            return;
        }

        $collectorDef = $container->getDefinition(MessengerCollector::class);

        foreach ($container->findTaggedServiceIds('messenger.receiver') as $id => $tags) {
            $aliases = [];

            foreach ($tags as $tagName => $tagParams) {
                if (isset($tagParams['alias'])) {
                    $aliases[] = $tagParams['alias'];
                }
            }

            $collectorDef->addMethodCall('addReceiver', [new Reference($id), $aliases]);
        }
    }
}
