<?php

namespace WakeOnWeb\Bundle\MessengerTestBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

/**
 * Class WakeOnWebMessengerTestExtension
 * @package WakeOnWeb\Bundle\MessengerTestBundle\DependencyInjection
 */
class WakeOnWebMessengerTestExtension extends Extension
{
    /**
     * @param array                                                   $configs
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     *
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        if ('test' === $container->getParameter('kernel.environment')) {
            $loader = new XmlFileLoader(
                $container,
                new FileLocator(__DIR__.'/../Resources/config')
            );
            $loader->load('services_test.xml');
        }
    }
}
