<?php

namespace WakeOnWeb\Bundle\MessengerTestBundle\Mock\Messenger;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\TransportInterface;

/**
 * Class Transport.
 */
class Transport implements TransportInterface
{
    /** @var array */
    public $sentMessages = [];

    public function receive(callable $handler): void
    {

    }

    public function stop(): void
    {

    }

    public function send(Envelope $envelope): Envelope
    {
        $this->sentMessages[] = $envelope;

        return $envelope;
    }
}
