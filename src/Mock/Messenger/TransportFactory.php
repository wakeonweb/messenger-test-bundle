<?php

namespace WakeOnWeb\Bundle\MessengerTestBundle\Mock\Messenger;

use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

/**
 * Class TransportFactory.
 */
class TransportFactory implements TransportFactoryInterface
{
    /**
     * @param string $dsn
     * @param array  $options
     *
     * @return \Symfony\Component\Messenger\Transport\TransportInterface
     */
    public function createTransport(string $dsn, array $options): TransportInterface
    {
        return new Transport();
    }

    /**
     * @param string $dsn
     * @param array  $options
     *
     * @return bool
     */
    public function supports(string $dsn, array $options): bool
    {
        return 0 === strpos($dsn, 'mock://');
    }
}
