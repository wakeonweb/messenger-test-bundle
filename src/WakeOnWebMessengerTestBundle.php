<?php

namespace WakeOnWeb\Bundle\MessengerTestBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use WakeOnWeb\Bundle\MessengerTestBundle\DependencyInjection\Compiler\MessengerCollectorPass;

/**
 * Class WakeOnWebMessengerTestBundle
 * @package WakeOnWeb\Bundle\MessengerTestBundle
 */
class WakeOnWebMessengerTestBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new MessengerCollectorPass());
    }
}
